USE [master]
GO

DROP DATABASE IF EXISTS Voltage

CREATE DATABASE Voltage
GO

USE Voltage
GO

CREATE TABLE [User] (
	[id] INT IDENTITY(1,1) PRIMARY KEY,
	[email] VARCHAR(100) NOT NULL,
	[password] VARCHAR(8) NOT NULL,
	[first_name] VARCHAR(50) NOT NULL,
	[last_name] VARCHAR(50) NOT NULL,
	[country] VARCHAR(50) NOT NULL,
	[address] VARCHAR(50) NOT NULL,
	[city] VARCHAR(50) NOT NULL,
	[state] VARCHAR(50) NOT NULL,
	[zip_code] VARCHAR(5) NOT NULL,
	[phone_number] VARCHAR(10) NOT NULL,
	[domain] VARCHAR(100) NULL,
	[plan_id] INT NULL,
	[credit_card_id] INT NOT NULL
)

CREATE TABLE [CreditCard] (
	[id] INT IDENTITY(1,1) PRIMARY KEY,
	[credit_card_number] VARCHAR(25) NOT NULL,
	[CVV2] VARCHAR(5) NOT NULL,
	[expiration_date] VARCHAR(5) NOT NULL
)

CREATE TABLE [Plans] (
	[id] INT IDENTITY(1,1) PRIMARY KEY,
	[name] VARCHAR(20) NOT NULL,
	[price] FLOAT NOT NULL,
	[type] VARCHAR(10) NOT NULL
)

INSERT INTO [Plans] VALUES ('Standard', 79.99, 'Hosting'),
						   ('Enhanced', 99.99, 'Hosting'),
						   ('Premium', 119.99, 'Hosting'),
						   ('Basic', 2.95, 'Wordpress'),
						   ('Plus', 4.95, 'Wordpress'),
						   ('Choice Plus', 5.95, 'Wordpress');
						  
INSERT INTO [CreditCard] VALUES ('354398867872', '152', '02/25');

INSERT INTO [User] VALUES ('jasotomayort@hotmail.com','1234', 'Juan', 'Sotomayor', 'Ecuador', 'Samanes 7', 'Guayaquil', 'Guayas', 9101, '0993435445', 'outlaw.com', 1, 1)


